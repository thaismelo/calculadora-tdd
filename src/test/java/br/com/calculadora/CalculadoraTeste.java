package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    private Calculadora calculadora;

    @BeforeEach
    public void setUp() {
        calculadora = new Calculadora();
    }

    @Test
    public void testaASomaDeDoisNumerosInteiros() {
        int resultado = calculadora.soma(1, 2);

        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testaASomaDeDoisNumerosFlutuantes() {
        double resultado = calculadora.soma(2.3, 3.4);

        Assertions.assertEquals(5.7, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosInteiros(){
        int resultado = calculadora.multiplicacao(1, 2);

        Assertions.assertEquals(2, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosNegativos(){
        int resultado = calculadora.multiplicacao(-1, 2);

        Assertions.assertEquals(-2, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosFlutuantes(){
        Double resultado = calculadora.multiplicacao(1.2, 1.1);

        Assertions.assertEquals(1.32, resultado);
    }

    @Test
    public void testarDivisaoDeDoisNumerosInteiros(){
        Double resultado = calculadora.divisao(2, 1);
        Assertions.assertEquals(2.0, resultado);
    }

    @Test
    public void testarDivisaoDeDoisNumerosFlutuantes(){
        Double resultado = calculadora.divisao(2.1, 5);
        Assertions.assertEquals(2.4, resultado);
    }

    @Test
    public void testarDivisaoDeDoisNumerosNegativos(){
        Double resultado = calculadora.divisao(-4, 5);
        Assertions.assertEquals(-1.0, resultado);
    }
}
