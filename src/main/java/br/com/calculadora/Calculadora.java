package br.com.calculadora;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculadora {

    public Calculadora() {

    }

    public int soma(int numero1, int numero2) {
        return numero1 + numero2;
    }

    public double soma(double numero1, double numero2) {
        Double resultado = numero1 + numero2;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(1, RoundingMode.HALF_EVEN);

        return bigDecimal.doubleValue();
    }

    public int multiplicacao(int numero1, int numero2) {
        return numero1 * numero2;
    }

    public double multiplicacao(double numero1, double numero2) {
        return numero1 * numero2;
    }

    public double divisao(int num1, int num2) {
        double resultado = 0;

        if (num2 > num1) {
            resultado = num2 / num1;
        } else {
            resultado = num1 / num2;
        }

        return resultado;
    }

    public double divisao(double num1, double num2) {
        double resultado = 0;

        if (num2 > num1) {
            resultado = num2 / num1;
        } else {
            resultado = num1 / num2;
        }

        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(1, RoundingMode.HALF_EVEN);

        return bigDecimal.doubleValue();
    }

}
